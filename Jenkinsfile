/*
	Author: Derek Wong
	Date: April 14, 2020
	Description: Pipeline for sample_code2
*/

pipeline {
    agent any
	parameters {
		booleanParam(defaultValue: false, description: 'Run Unit Tests', name: 'TEST')
	}
    stages {
        stage('Build') {
            steps {
                sh 'echo Installing the Python Requirements...'
				sh 'pip install -r requirements.txt'
				sh 'echo Requirements complete.'
            }
        }
		
		stage('Code Quality') {
            steps {
                sh 'pylint-fail-under --fail_under 7.0 *.py'
            }
        }
		
		stage('Code Quantity') {
            steps {
				sh "find . -name '*.py' | wc -l"
            }
        }
		
		stage('Test') {
			when {
				expression { 
					params.TEST
				}
			}
            steps {
                sh 'python3 test_book_manager.py'
				sh 'coverage run --omit */site-packages/*,*/dist-packages/* test_book_manager.py'
            }
			post {
				always {
					junit 'test-reports/*.xml'
					sh """coverage report"""
				}
                }
        }
		
		stage('Package') {
			steps {
				sh 'zip package.zip *.py'
			}
			post {
				always {
					archiveArtifacts artifacts: 'package.zip'
				}
			}
		}
    }
}
